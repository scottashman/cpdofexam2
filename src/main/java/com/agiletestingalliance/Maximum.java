package com.agiletestingalliance;

public class Maximum {

    public int whichIsBigger(int first, int second) {
        if (second > first) {
            return second;
        } else {
            return first;
	}
    }

}
