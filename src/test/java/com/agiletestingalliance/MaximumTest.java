package com.agiletestingalliance;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;

public class MaximumTest extends Mockito{

    @Test
    public void testMaximum() throws Exception {

        int k= new Maximum().whichIsBigger(1,3);
        assertEquals("Which is bigger",k,3);
        
    }
}
