package com.agiletestingalliance;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;

public class DurationTest extends Mockito{

    @Test
    public void testDur() throws Exception {

        String result = new Duration().dur();
        assertTrue(result.toString().contains("CP-DOF is designed specifically"));
        
    }
}
