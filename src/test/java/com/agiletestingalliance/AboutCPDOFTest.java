package com.agiletestingalliance;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;

public class AboutCPDOFTest extends Mockito{

    @Test
    public void testDesc() throws Exception {

        String result = new AboutCPDOF().desc();
        assertTrue(result.toString().contains("CP-DOF certification program covers end"));
        
    }
}
